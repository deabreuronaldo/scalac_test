package githubservice

import githubservice.ServiceError.ImpossibleToBuildURL
import play.api.libs.json.{JsNumber, JsObject, JsValue}
import play.api.libs.ws.{WSClient, WSResponse}

import scala.concurrent.{ExecutionContext, Future}

object RequestExecutor {

  private val invalidSearchValue: Int = 0

  private val githubRateLimmitVerificationAPI = "https://api.github.com/rate_limit"

  private def getLimits(jsonResponse: JsValue): Int = {
    jsonResponse match {
      case JsObject(underlying) =>
        underlying.get("rate") match {
          case Some(JsObject(underlying)) => underlying.get("remaining") match {
            case Some(JsNumber(value)) => value.toInt
            case _ => invalidSearchValue
          }
          case _ => invalidSearchValue
        }
      case _ => invalidSearchValue
    }

  }

  def getRateLimit(ws: WSClient)(implicit ec: ExecutionContext) : Future[Int] = {
     val response: Future[WSResponse] = ws.url(githubRateLimmitVerificationAPI)
      .withHttpHeaders(defaultHeaderMapping.toSeq:_*).get()
    response.map( realResponse => if (realResponse.status == 200 ) getLimits(realResponse.json) else invalidSearchValue )
  }

  def getGitHubAPI(ws: WSClient, apiURL: GitHubAPI)(implicit ec: ExecutionContext) : Future[WSResponse] = {
    apiURL.buildURL.toOption match {
      case Some(url) =>
        if (apiURL.hasHeaders)
          ws.url(url).withHttpHeaders(apiURL.headers.toSeq: _*).get()
        else
          ws.url(url).get
      case _ => Future.failed(ImpossibleToBuildURL())
    }
  }
}
