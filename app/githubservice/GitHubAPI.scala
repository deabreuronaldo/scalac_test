package githubservice

import play.api.libs.json.JsValue
import play.api.libs.ws.WSResponse

import scala.concurrent.{ExecutionContext, Future}

trait GitHubAPI {
  val baseHTTPURL: String = "https://api.github.com/"
  val apiURL: String
  def beginingURL: URL = baseHTTPURL + apiURL

  def buildURL: Either[ServiceError, URL]

  val headers: Map[String, String]
  def hasHeaders: Boolean = headers.nonEmpty

  def jsonProcessor(response: Future[WSResponse])
                   (implicit ec: ExecutionContext): Future[JsValue] =
    response.map(_.json)

}
