package githubservice

import play.api.libs.json.JsString
import play.api.libs.ws.WSClient

import scala.concurrent.{ExecutionContext, Future}

case class Repositories(org: String) extends GitHubAPI {
  override val apiURL: String = "orgs/"
  val postPositionURL: String = "/respos"
  override val headers: Map[String, String] = defaultHeaderMapping

  override def buildURL: Either[ServiceError, URL] =
    if (org.nonEmpty) Right(beginingURL +  org + postPositionURL)
    else Left(ServiceError.InvalidURL())

  def getListOfRepositories(ws: WSClient)(implicit ec: ExecutionContext): Future[Seq[String]] =
    jsonProcessor(RequestExecutor.getGitHubAPI(ws, this))
      .collect(jsValue => jsValue.\\("full_name")
        .collect({case JsString(value) => value})
        .toSeq.distinct.map(_.split("/").last))

}
