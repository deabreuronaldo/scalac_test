package githubservice

import model.ResumedContributions
import play.api.libs.json.{JsArray, JsNumber, JsObject, JsString}
import play.api.libs.ws.WSClient

import scala.concurrent.{ExecutionContext, Future}

case class Contributors (org: String, repo: String) extends GitHubAPI {
  require(repo.nonEmpty)
  override val apiURL: String = "repos/"
  override val headers: Map[String, String] = defaultHeaderMapping
  val postPositionURL: String = "/contributors"

  override def buildURL: Either[ServiceError, URL] =
    if (org.nonEmpty && repo.nonEmpty) Right(beginingURL +  org + "/" + repo + postPositionURL)
    else Left(ServiceError.InvalidURL())

  def getListOfContributions(ws: WSClient)(implicit ec: ExecutionContext): Future[Seq[ResumedContributions]] =
    jsonProcessor(RequestExecutor.getGitHubAPI(ws, this))
      .map(jsValue=> jsValue match {
          case JsArray(value) =>
            val internalResult = for (el <- value) yield el match {
                case JsObject(obj) =>
                  (obj.get("login"), obj.get("contributions")) match {
                    case (Some(name:JsString), Some(contributions:JsNumber)) =>
                      Some(ResumedContributions(name.value, repo, contributions.value.toInt))
                    case _ => None
                  }
                case _ => None
              }
            internalResult.collect({case Some(data) => data}).toSeq
          case _ => Seq.empty
        })

}
