package object githubservice {

  type URL = String

  val defaultHeaderMapping = Map("Accept" -> "application/vnd.github.v3+json")

}
