package githubservice

trait ServiceError


object ServiceError {

  case class InvalidURL() extends ServiceError
  case class ImpossibleToBuildURL() extends Exception with ServiceError
}