package githubservice.analysis

import model.{ResumedContributions, SummedContributions}

object SummarizedData {

  def summarize( workingData: Seq[ResumedContributions]) = {
    type CountList = Map[String, Long]
    workingData.foldLeft(Map.empty:CountList)((count, el) =>
      if (count.keySet.contains(el.name)) {
        val updatedCount = count.get(el.name).map( _ + el.contributions.toLong).getOrElse(0L)
          count - el.name ++ Map(el.name -> updatedCount)
      }
      else count ++ Map(el.name -> el.contributions)
    ).map(el => SummedContributions(el._1, el._2)).toSeq.sortBy(el => el.totalContributions)
  }

}
