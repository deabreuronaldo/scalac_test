

val reposExampleStr = scala.io.Source.fromFile("/Users/ronaldo/Project/Scalac/GithubAssighment/app/githubservice/githubRepoResponse.json").mkString

val contributorsExampleStr = scala.io.Source.fromFile("/Users/ronaldo/Project/Scalac/GithubAssighment/app/githubservice/githubcontributors.json").mkString

import play.api.libs.json.{JsArray, JsNumber, JsObject, JsString, Json}

val parsedJsonRepos = Json.parse(reposExampleStr)

parsedJsonRepos match {
  case JsObject(underlying) => println("JSObject")
  case JsArray(value) => println("JsArray")
  case _ => println("None of the above")
}

parsedJsonRepos.\\("full_name").map(el => el match {
  case JsObject(underlying) => println("JSObject")
  case JsArray(value) => println("JsArray")
  case JsString(value) => println(s"JsString with value $value --- ", el.toString)
  case _ => println("None of the above")
}
)


parsedJsonRepos.\\("full_name").toSeq.collect({case JsString(value) => value}).map(_.split("/").last).foreach(println)

val parsedJsonContributors = Json.parse(contributorsExampleStr)

parsedJsonContributors match {
  case JsObject(underlying) => println("JSObject")
  case JsArray(value) => println("JsArray")
  case _ => println("None of the above")
}


case class Contributions (contributor: String, contributions: Int)

parsedJsonContributors match {
  case JsArray(value) =>
    (for (el <- value)
        yield el match {
          case JsObject(obj) =>
            (obj.get("login"), obj.get("contributions")) match {
              case (Some(x:JsString), Some(y:JsNumber)) => Some(Contributions(x.value, y.value.toInt))
              case _ => None
            }
          case _ => None
        }
      ).collect({case Some(x) => x}).toSeq.sortBy(_.contributions)
  case _ => Seq.empty
}

parsedJsonContributors.\\("login").foreach(println)
parsedJsonContributors.\\("contributions").foreach(println)
