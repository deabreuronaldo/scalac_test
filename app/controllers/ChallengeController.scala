package controllers

import githubservice.analysis.SummarizedData
import githubservice.{Contributors, Repositories, RequestExecutor}
import model.SumarizedResult
import play.api.Configuration
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}

import javax.inject.Inject
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class ChallengeController @Inject()(ws: WSClient, cc:ControllerComponents, conf: Configuration) extends AbstractController(cc){

  val defaultTimeout = 10.seconds

  def organizationSummarizer(organization: String) = Action {
    implicit request: Request[AnyContent] => {
      val result = Await.result(RequestExecutor.getRateLimit(ws), defaultTimeout)
      if (organization.nonEmpty && result >= 2) {
          val listOfRepositories = Await.result(Repositories(organization).getListOfRepositories(ws), defaultTimeout)
          val other = listOfRepositories.
            flatMap(repository => Await.result(Contributors(organization, repository).getListOfContributions(ws), defaultTimeout))
          val summedResults = SummarizedData.summarize(other)
          val result = SumarizedResult(organization = organization, contributions = summedResults.toArray)
          Ok(Json.toJson(result))
      }
      else BadRequest
    }
  }

}
