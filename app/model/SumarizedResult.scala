package model

case class SumarizedResult(contributions: Array[SummedContributions], organization: String)

