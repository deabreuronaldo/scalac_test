import play.api.libs.json.{Json, Writes}

package object model {

  implicit val writesSummedContributions = new Writes[SummedContributions] {
    def writes(sum: SummedContributions) = Json.obj(
      "name" -> sum.name,
      "totalContributions" -> sum.totalContributions
    )
  }

  implicit val writesSumarizedResult: Writes[SumarizedResult] = new Writes[SumarizedResult] {
    def writes(summarizedData: SumarizedResult) = Json.obj(
      "organization" -> summarizedData.organization,
      "listedContributors" -> summarizedData.contributions
    )

  }
}
