package model

case class SummedContributions (name: String, totalContributions:Long)
