package model

case class ResumedContributions(name: String, repository: String, contributions: Int)

