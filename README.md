# Scalac challenge for Github

To follow the steps in this tutorial, you will need the correct version of Java and sbt. The template requires:

* Java Software Developer's Kit (SE) 1.8 or higher
* sbt 1.3.4 or higher.

To check your Java version, enter the following in a command window:

```bash
java -version
```

To check your sbt version, enter the following in a command window:

```bash
sbt sbtVersion
```

## Notes
 * Couldn't make the Unit Tests Play integration with Scalatest is not working properly. An I couldn't make in time

 * Left a folder for scraping Testes where I tested Extraction code

 * Couldn't test the webService caller, but as it is a standard play component shouldn't be an issue.

 * Tested the scraped JSON over the JsValues of Play to make sure extraction would occur as I hoped.

 * Didn't have the time to make the config for GH_TOKEN be available through Configuration, and even if I did. O didn't understand the meaning of it.


## Build and run the project

This example Play project was created from a seed template. It includes all Play components and an Akka HTTP server. The project is also configured with filters for Cross-Site Request Forgery (CSRF) protection and security headers.

To build and run the project:

1. Build the project. Enter: `sbt run`. The project builds and starts the embedded HTTP server. Since this downloads libraries and dependencies, the amount of time required depends partly on your connection's speed.

3. After the message `Server started, ...` displays, enter the following URL in a browser: <http://localhost:8080>

