
lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := """scalac-challenge-github""",
    organization := "io.scalac",
    version := "0.1.0-SNAPSHOT",
    scalaVersion := "2.13.1",
    libraryDependencies ++= Seq(
      guice,
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
      ws,
      ehcache,
),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings",
      "-Werror"
    )
  )
